<?php

namespace App\Controllers;
use App\Models\Pokemon;
use App\Models\Pokedex;

class AdicionarPokedex {

    private $pokemon;
    private $pokedex;

    public function __construct(Pokemon $pokemon, Pokedex $pokedex)
    {
        $this->pokemon = $pokemon;
        $this->pokedex = $pokedex;
    }


    public function adiciona(){

        $permissao = $this->pokemon->podeSerSalvo();

        if($permissao) {
            $this->pokedex->adiciona($this->pokemon);
            return true;
        } else {
            return false;
        }

    }

}