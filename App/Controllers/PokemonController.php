<?php

namespace App\Controllers;

use MF\Controller\Action;
use MF\Model\Container;
use App\Validators\ValidaArray;
use App\Controllers\AdicionarPokedex;

class PokemonController extends Action
{
   public $view;

   public function __construct()
   {
      $this->analiseArray = new ValidaArray();
   }

   public function listarPokemons()
   {
      $pokemon = Container::getModel('Pokemon');
      $pokemons = $pokemon->listar();
      @$this->view->pokemons = $pokemons;

      $this->render('listarPokemons');
   }

   public function analisePokemon()
   {

      if ($this->analiseArray->validaSetagem($_GET, ['id'])) {
         $pok = Container::getModel('Pokemon');
         $pok->__set('id', $_GET['id']);
         $pokemon = $pok->listar();
         $descPeso = $this->analisePeso($pokemon[0]['peso']);
         $descAltura = $this->analiseAltura($pokemon[0]['altura']);
         $descForca = $this->analiseForca($pokemon[0]['forca']);
         $pokemon[0]['descPeso'] = $descPeso;
         $pokemon[0]['descAltura'] = $descAltura;
         $pokemon[0]['descForca'] = $descForca;
         $this->view->pokemon = $pokemon;

         $this->render('analisePokemon');
      } else {
         echo "Id não informado";
      }
   }


   public function analisePeso($peso)
   {

      if ($peso >= 30) {
         return 'Peso Pesado';
      } else if ($peso <= 29 && $peso >= 10) {
         return 'Peso Médio';
      } else {
         return 'Peso Leve';
      }
   }

   public function analiseAltura($altura)
   {
      if ($altura >= 5) {
         return 'Alto';
      } else if ($altura <= 4.9 && $altura >= 1) {
         return 'Médio';
      } else {
         return 'Baixo';
      }
   }

   public function analiseForca($forca)
   {
      if ($forca >= 50) {
         return 'Forte';
      } else if ($forca <= 49 && $forca >= 20) {
         return 'Razoável';
      } else {
         return 'Fraco';
      }
   }

   public function treinarPokemon()
   {
      if ($this->analiseArray->validaSetagem($_GET, ['id', 'tipo'])) {
         $forca = $this->geraForca($_GET['tipo']);
         $vida = $this->geraVida($_GET['tipo']);
         $pok = Container::getModel('Pokemon');
         $pok->__set('id', $_GET['id']);
         $pok->__set('forca', $forca);
         $pok->__set('vida', $vida);
         $pok->insereForcaVida();
         header("Location: /analisePokemon?id={$_GET['id']}");
      } else {
         echo "Erro";
      }
   }


   public function adicionarPokedex(){
      try{
         $pok = Container::getModel('Pokemon');
         $pokdx = Container::getModel('Pokedex');
         $pok->__set('id', $_GET['id']);
         $adicionarPokedex = new AdicionarPokedex($pok, $pokdx);
      
      }catch(\Exception $e){

      }
   }

   public function geraForca($tipo)
   {

      switch ($tipo) {
         case 'bug':
            return rand(1, 50);
         case 'dark':
            return rand(1, 73);
         case 'dragon':
            return rand(1, 130);
         case 'electric':
            return rand(1, 90);
         case 'fairy':
            return rand(1, 75);
         case 'fire':
            return rand(1, 80);
         case 'fighting':
            return rand(1, 85);
         case 'flying':
            return rand(1, 60);
         case 'ghost':
            return rand(1, 78);
         case 'grass':
            return rand(1, 40);
         case 'ground':
            return rand(1, 35);
         case 'ice':
            return rand(1, 87);
         case 'normal':
            return rand(1, 25);
         case 'poison':
            return rand(1, 30);
         case 'psychic':
            return rand(1, 89);
         case 'rock':
            return rand(1, 55);
         case 'steel':
            return rand(1, 70);
         case 'water':
            return rand(1, 95);
         default:
            return 0;
      }
   }

   public function geraVida($tipo)
   {

      switch ($tipo) {
         case 'bug':
            return rand(1, 80);
         case 'dark':
            return rand(1, 53);
         case 'dragon':
            return rand(1, 130);
         case 'electric':
            return rand(1, 60);
         case 'fairy':
            return rand(1, 55);
         case 'fire':
            return rand(1, 65);
         case 'fighting':
            return rand(1, 75);
         case 'flying':
            return rand(1, 45);
         case 'ghost':
            return rand(1, 52);
         case 'grass':
            return rand(1, 50);
         case 'ground':
            return rand(1, 90);
         case 'ice':
            return rand(1, 62);
         case 'normal':
            return rand(1, 54);
         case 'poison':
            return rand(1, 58);
         case 'psychic':
            return rand(1, 61);
         case 'rock':
            return rand(1, 100);
         case 'steel':
            return rand(1, 110);
         case 'water':
            return rand(1, 41);
         default:
            return 0;
      }
   }
}
