<?php

namespace App\Models;

use MF\Model\Model;
use App\Models\Pokemon;


class Pokedex extends Model
{


    public function __get($atributo)
    {
        return $this->$atributo;
    }

    public function __set($atributo, $valor)
    {
        $this->$atributo = $valor;
    }


    public function adiciona(Pokemon $pok)
    {

        $query = "insert into pokedex(idpokemon)values(:nome)";
        $stmt = $this->db->prepare($query);
        $stmt->bindValue(':nome', $this->__get('nome'));
        $stmt->bindValue(':altura', $this->__get('altura'));
        $stmt->bindValue(':peso', $this->__get('peso'));
        $stmt->bindValue(':categoria', $this->__get('categoria'));
        $stmt->bindValue(':imagem', $this->__get('imagem'));
        $stmt->execute();
        return $this;
    }
}
