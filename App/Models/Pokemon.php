<?php

namespace App\Models;

use MF\Model\Model;

class Pokemon extends Model
{
    private $id;
    private $nome;
    private $altura;
    private $peso;
    private $categoria;
    private $imagem;
	private $forca;
	private $vida;


    public function __get($atributo)
	{
		return $this->$atributo;
	}

	public function __set($atributo, $valor)
	{
		$this->$atributo = $valor;
	}

    public function salvar()
	{

		if (!empty(trim($this->__get('nome')))) {
			$query = "insert into pokemons(nome, altura, peso, categoria, imagem)values(:nome, :altura, :peso, :categoria, :imagem)";
			$stmt = $this->db->prepare($query);
			$stmt->bindValue(':nome', $this->__get('nome'));
			$stmt->bindValue(':altura', $this->__get('altura'));
			$stmt->bindValue(':peso', $this->__get('peso'));
			$stmt->bindValue(':categoria', $this->__get('categoria'));
			$stmt->bindValue(':imagem', $this->__get('imagem'));
			$stmt->execute();
			return $this;
		
		} else {
			return false;
		}
	}

    public function listar()
	{
		$where = '';
		if($this->__get('id') != null) {
			$id = $this->__get('id');
			$where = "where id = $id";
		}

		$query = "
			select 
				  *
			from pokemons
			$where";

		$stmt = $this->db->prepare($query);
		$stmt->execute();

		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	public function insereForcaVida()
	{
		$query = "update pokemons set forca = :forca, vida = :vida where id = :id";
		$stmt = $this->db->prepare($query);
		$stmt->bindValue(':id', $this->__get('id'));
		$stmt->bindValue(':forca', $this->__get('forca'));
		$stmt->bindValue(':vida', $this->__get('vida'));
		$stmt->execute();
		return $this;
	}

	public function podeSerSalvo(){
		$forca = $this->__get('forca');
		$vida = $this->__get('vida');

		$total = $forca + $vida;

		if($total > 200) {
			return false;
		} else {
			return true;
		}


	}

}