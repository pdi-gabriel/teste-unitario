<?php

namespace App;

use MF\Init\Bootstrap;

class Route extends Bootstrap {

	protected function initRoutes() {

		$routes['home'] = array(
			'route' => '/',
			'controller' => 'indexController',
			'action' => 'index'
		);

		$routes['analisePokemon'] = array(
			'route' => '/analisePokemon',
			'controller' => 'PokemonController',
			'action' => 'analisePokemon'
		);

		$routes['listarPokemons'] = array(
			'route' => '/listarPokemons',
			'controller' => 'PokemonController',
			'action' => 'listarPokemons'
		);

		$routes['treinarPokemon'] = array(
			'route' => '/treinarPokemon',
			'controller' => 'PokemonController',
			'action' => 'treinarPokemon'
		);


		$this->setRoutes($routes);
	}

}

?>