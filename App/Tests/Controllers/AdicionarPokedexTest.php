<?php

use App\Controllers\AdicionarPokedex;
use PHPUnit\Framework\TestCase;
use App\Models\Pokemon;
use App\Models\Pokedex;

class AdicionarPokedexTest extends TestCase
{

    /**
     * @test
     */

    public function shouldNotSaveWhenPokemonIsNotTrue()
    {

        $pokemon = $this->createMock(Pokemon::class);
        $pokemon->method('podeSerSalvo')->willReturn(false);

        $pokedex = $this->createMock(Pokedex::class);
        $pokedex->expects($this->never())->method('adiciona');

        $adicionarPokedex = new AdicionarPokedex($pokemon, $pokedex);

        $adicionarPokedex->adiciona();
    }


    /** 
     * @test
     */

    public function shouldSaveWhenPokemonIsTrue()
    {

        $pokemon = $this->createMock(Pokemon::class);
        $pokemon->method('podeSerSalvo')->willReturn(true);

        $pokedex = $this->createMock(Pokedex::class);
        $pokedex->expects($this->once())->method('adiciona');

        $adicionarPokedex = new AdicionarPokedex($pokemon, $pokedex);

        $adicionarPokedex->adiciona();
    }
}
