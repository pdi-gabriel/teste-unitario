<?php

use PHPUnit\Framework\TestCase;
use App\Controllers\PokemonController;
use App\Models\Pokemon;
use App\Validators\ValidaArray;

class PokemonControllerTest extends TestCase
{

    /**
     * 
     * @dataProvider valueProviderPeso
     */
    public function testAnalisePeso($value, $expectedResult)
    {

        $controller = new PokemonController();
        $res = $controller->analisePeso($value);

        $this->assertEquals($expectedResult, $res);
    }


    /**
     * @dataProvider valueProviderAltura
     */
    public function testAnaliseAltura($value, $expectedResult)
    {

        $controller = new PokemonController();
        $res = $controller->analiseAltura($value);
        $this->assertEquals($expectedResult, $res);
    }


    /**
     * @dataProvider valueProviderForca
     */
    public function testAnaliseForca($value, $expectedResult)
    {

        $controller = new PokemonController();
        $res = $controller->analiseForca($value);
        $this->assertEquals($expectedResult, $res);
    }

    public function testTreinarPokemon()
    {
        $validator = $this->createMock(PokemonController::class);
        $validator->expects($this->once())->method('treinarPokemon');

    }


    public function valueProviderPeso()
    {
        return [
            "shouldBePesoPesadoCasePesoIsMoreOrEqualsThen30" => ['value' => 30, 'expectedResult' => "Peso Pesado"],
            "shouldBePesoMedioCasePesoIsBetween29And10" => ['value' => 20, 'expectedResult' => "Peso Médio"],
            "shouldBePesoLeveCasePesoIsLessThen10" => ['value' => 9, 'expectedResult' => "Peso Leve"]
        ];
    }



    public function valueProviderAltura()
    {
        return [
            "shouldBeAltoCaseAlturaIsMoreOrEqualsThen5" => ['value' => 5, 'expectedResult' => "Alto"],
            "shouldBeMedioCaseAlturaIsBetween4.9And1" => ['value' => 3, 'expectedResult' => "Médio"],
            "shouldBeBaixoCaseAlturaIsLessThen1" => ['value' => 0.7, 'expectedResult' => "Baixo"]
        ];
    }


    public function valueProviderForca()
    {
        return [
            "shouldBeForteCaseForcaIsMoreOrEqualsThen50" => ['value' => 50, 'expectedResult' => "Forte"],
            "shouldBeRazoavelCaseForcaIsBetween49And20" => ['value' => 27, 'expectedResult' => "Razoável"],
            "shouldBeFracoCaseForcaIsLessThen20" => ['value' => 15, 'expectedResult' => "Fraco"]
        ];
    }
}
