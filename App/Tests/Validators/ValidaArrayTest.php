<?php

use PHPUnit\Framework\TestCase;
use App\Validators\ValidaArray;

class ValidaArrayTest extends TestCase {

    /**
     * 
     * @dataProvider valueProvider
     */
    public function testAnalisePokemon($value, $resultResult) {
        $validators = new ValidaArray();
        $res = $validators->validaSetagem($value['array'], $value['atributo']);

        $this->assertEquals($resultResult, $res);

    }


    public function valueProvider(){
        return [
            "shouldBeValidWhenHaveAttributeInArray" => ['value' => ['array' => ["teste" => 1, "teste2" => 2], 'atributo' => ['teste']], 'expectedResult' => true],
            "shouldBeInvalidWhenNotHaveOneAttributeInArray" => ['value' => ['array' => ["teste" => 1, "teste2" => 2], 'atributo' => ['teste', 'teste3']], 'expectedResult' => false]
        ];
    }

}