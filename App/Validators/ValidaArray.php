<?php

namespace App\Validators;

class ValidaArray {

    /**
     * 
     * @param array $arr
     * @param array $atr
     * 
     */

    public function validaSetagem($arr, $atr){
        $res = [];
        foreach ($atr as $i => $v) {
            if(isset($arr["$v"]) && $arr[$v] !== '') {
                $res = ["sucesso" => true];
            } else {
                $res = ["erro" => true];
            }
        }

        if(isset($res['erro'])) {
            return false;
        } else {
            return true;
        }

    }


}
