# teste-unitario

Projeto destinado para demonstração de entendimento sobre teste unitários utilizando phpunit.

## Sobre o projeto

O pequeno sistema desenvolvido utilizando controllers, validators e models, para aplicar testes em seus métodos.

Caminho dos testes: App/Tests
Versão PHPUNIT: 9.0.0

O sistema se trata de um CRUD de Pokémons, onde se pode listar, treinar, analisar e adicionar em uma pokédex. Seus métodos possuem regras para as análises e para adicionar determinados pokémons em uma pokédex baseado em sua força.

## Métodos e anotações do PHPUNIT aplicados

- [ ] [createMock()](https://phpunit.readthedocs.io/pt_BR/latest/test-doubles.html) com `method()`, `willReturn()`, `expects`, `never()`, `once()`.
- [ ] [assertEquals](https://phpunit.readthedocs.io/pt_BR/latest/assertions.html?highlight=assert#assertequals).
- [ ] [dataProvider](https://phpunit.readthedocs.io/pt_BR/latest/annotations.html#dataprovider).


## Como testar:

Os testes foram realizados no terminal, primeiro digite o diretório que o phpunit foi instalados, já que instalei pelo composer, o phpunit está localizado na pasta vendor:

```
vendor/bin/phpunit

```

Após, na frente do diretório do phpunit, digite o diretório ou arquivo de testes que deseja realizar:

```
vendor/bin/phpunit App/Tests/Controllers/AdicionarPokedexTest.php

```
E pronto! Agora é só executar o comando e receber o resultado dos teste via console:


```
PHPUnit 9.0.0 by Sebastian Bergmann and contributors.

.                                                                   1 / 1 (100%)

Time: 00:00.138, Memory: 4.00 MB

OK (1 test, 1 assertion)
```
